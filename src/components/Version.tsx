import React from 'react';
import {Dispatch} from 'redux';
import {MavenVersionStatus, setValue, useAppSelector, useAppDispatch} from '@site/src/store'

interface LatestVersion {
    isSnapshot: boolean,
    version: string
}

const goFancyMaven = 'https://maven.gofancy.wtf'
const repository = 'releases'
const group = 'wtf/gofancy/koremods'

async function fetchLatestVersion(name: string): Promise<string> {
    let response = await fetch(`${goFancyMaven}/api/maven/latest/version/${repository}/${group}/${name}`)
    if (response.ok) {
        let json = await response.json()
        let data = json as LatestVersion
        return data.version
    }
    throw new Error(`Could not fetch ${name} version`)
}

async function fetchAndDispatchLatestVersion(dispatch: Dispatch, name: string) {
    dispatch(setValue({
        name: name,
        value: {
            type: MavenVersionStatus.PENDING,
            value: undefined
        }
    }))
    
    let version
    try {
        version = await fetchLatestVersion(name)
    } catch(e) {
        console.error(e)
        version = undefined
    }
    dispatch(setValue({
        name: name,
        value: {
            type: MavenVersionStatus.RESOLVED,
            value: version
        }
    }))
}

export function getLatestVersion(name: string): string {
    const version = useAppSelector(state => state.versions[name])
    const dispatch = useAppDispatch()
    
    if (!version) fetchAndDispatchLatestVersion(dispatch, name)
    
    return version?.value || '<version>'
}
