import {TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux'
import {configureStore, createSlice, PayloadAction} from '@reduxjs/toolkit'

export enum MavenVersionStatus {
    PENDING,
    RESOLVED
}

interface MavenVersionResolvable {
    type: MavenVersionStatus,
    value: any
}

export interface VersionPayload {
    name: string,
    value: MavenVersionResolvable
}

export const versionsSlice = createSlice({
    name: 'versions',
    initialState: {},
    reducers: {
        setValue: (state, action: PayloadAction<VersionPayload>) => {
            state[action.payload.name] = action.payload.value
        }
    }
})

export const {setValue} = versionsSlice.actions

export const store = configureStore({
    reducer: {
        versions: versionsSlice.reducer
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export const useAppDispatch: () => AppDispatch = useDispatch

