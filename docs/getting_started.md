---
id: getting_started
title: Getting Started
---

## Terminology

Please familiarize yourself with the basic terms of Koremods. We'll be using them quite often throughout
the documentation.

### 📦 Script Pack
A Script Pack represents a bundle of Koremods kotlin scripts. It can exist in the form of a directory,
`zip` or `jar` archive. Contained source scripts must use the `core.kts` file extension,
or `jar` for pre-compiled scripts.

### 📄 Script
A single Script can be any valid Kotlin Script file which has the `core.kts` file extension,
and therefore uses the Koremods Script Definition.

### 🧩 Frontend
A frontend represents an adapter between the target platform and Koremods Script. It uses the Koremods launch API
to configure and handle the script loading process.

## Modules and Libraries

Koremods is split into multiple libraries for easier management, debugging, and sharing common code.

### Koremods Script

[Koremods Script](https://gitlab.com/gofancy/koremods/koremods) is the core library of Koremods, containing the
Kotlin Script definition, configuration classes, and code related to discovering and evaluating scripts. It's supposed
to be platform-independent, serving as a backend for frontend implementations.

### Koremods Gradle

[Koremods Gradle](https://gitlab.com/gofancy/koremods/koremods-gradle) is a Gradle Plugin used to pre-compile scripts
during packaging. It automatically detects valid script packs in source sets, compiles the scripts during build-time
and replaces each script source in the jar with the script compiler ouput.

By pre-compiling scripts, we don't have to ship the heavy kotlin embedded compiler (~50MB) with the runtime,
and compiled scripts can be loaded into the JVM instantly without the need of additional preprocessing,
just like regular classes.

### Koremods Modlauncher

[Koremods Modlauncher](https://gitlab.com/gofancy/koremods/koremods-modlauncher) is the current primary frontend for
Koremods. It allows users to run Koremods alongside [Modlauncher](https://github.com/McModLauncher/modlauncher),
MinecraftForge's associated library for "launching Java programs with in-flight transformation ability".
The following pages of this documentation will guide you to configure and set up an environment using this frontent.
