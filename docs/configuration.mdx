---
id: configuration
title: Configuration
---

import Admonition from '@theme/Admonition'
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Koremods Gradle

### Compiler Worker Daemon

The embedded Kotlin Script Compiler can take a long time to load into the JVM due to its large size,
slowing down initial script compilation significantly. Subsequent scripts are then compiled up to 10-15 times faster.
This can quickly become an issue when compiling multiple script sourceSets or during development,
when you’ll want to reload and apply your transformer changes as quickly as possible.

As a solution, Koremods Gradle creates a separate [process](https://docs.gradle.org/current/userguide/worker_api.html)
for compiling scripts which persists across builds. While the daemon takes an extra second to start up,
keeping the compiler loaded speeds up script compilation times significantly.

<Admonition type="caution">
  The Worker Daemon is managed by Gradle, and will be terminated whenever memory gets low.
  Please make sure you allocate Gradle enough heap to keep it alive.
</Admonition>

#### Daemon memory

By default, the daemon’s max heap is set to `512M`, which is enough for most users.
However, compiling more scripts (>10) might require bumping the limit.

````mdx-code-block
<Tabs>
<TabItem value="groovy" label="Groovy DSL">

```groovy
koremods {
    workerDaemonOptions {
        it.maxHeapSize = '1G' // Increase worker daemon heap size as needed
    }
}
```

</TabItem>
<TabItem value="kotlin" label="Kotlin DSL">

```kotlin
koremods {
    workerDaemonOptions {
        maxHeapSize = "1G" // Increase worker daemon heap size as needed
    }
}
```

</TabItem>
</Tabs>
````

#### Optimizing CI Environments

If you're only compiling one source set in a CI environment, it's more efficient to disable the worker daemon
altogether and save a bit of time it would take to start up.
One way to approach this would be setting the `useWorkerDaemon` based on an environment variable of your choice.

````mdx-code-block
<Tabs>
<TabItem value="groovy" label="Groovy DSL">

```groovy
koremods {
    useWorkerDaemon = false // Disable worker daemon
}
```

</TabItem>
<TabItem value="kotlin" label="Kotlin DSL">

```kotlin
koremods {
    useWorkerDaemon.set(false) // Disable worker daemon
}
```

</TabItem>
</Tabs>
````

## Koremods Script

### Creating a Script Pack

Script packs are defined in a resource file located at `META-INF/koremods.conf` which uses the
[**HOCON**](https://github.com/lightbend/config/blob/main/README.md#using-hocon-the-json-superset) format,
a superset of JSON.

#### Namespace ID

The namespace ID is used to uniquely identify the script pack. It should be a unique, lower snake_case string.

<Admonition type="info">
  If your script pack is embedded inside a FML mod, its namespace must match the modid of that mod.
</Admonition>

```json
namespace = "examplepack"
```

#### Scripts

An array of paths to your script sources, relative to the resources root directory. Script file names must only
contain **alphanumeric** characters and their extension must be `core.kts`.

```json
scripts = [
  "scripts/transformExample.core.kts"
]
```
