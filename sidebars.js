// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
module.exports = {
  sidebar: [
    'introduction',
    'getting_started', 
    'installation',
    'configuration',
    'writing_a_script'
  ]
};
